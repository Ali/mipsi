import numpy as np
from numpy import loadtxt
from sklearn import datasets

from classification.classification_algorithm import Classifier


def main(file_input_arg='./input.txt', file_output_arg='./output.txt'):
    file_input = file_input_arg
    file_output = file_output_arg

    # делим на подмассивы по 4
    numbers = np.array(list(map(float, loadtxt(file_input, delimiter=',', unpack=False)))).reshape(-1, 4)

    type_of_iris = np.array(["Setosa", "Versicolor", "Verginica"])

    # Загрузка данных
    iris = datasets.load_iris()
    X = iris.data
    y = iris.target

    # Создание и обучение классификатора
    classifier = Classifier()
    classifier.train(X, y)

    predictions = classifier.predict(numbers)

    for sample, prediction in zip(numbers, predictions):
        print(f"Sample {sample} is classified as class {type_of_iris[prediction]}")

    with open(file_output, 'w+', encoding="utf-8") as file:
        for sample, prediction in zip(numbers, predictions):
            file.write(f"Sample {sample} is classified as class {type_of_iris[prediction]}")


if __name__ == "__main__":
    main()
