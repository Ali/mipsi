from classification.classifier import main

input_file = "input.txt"
output_file = "output.txt"


def write_and_execute_input_data(str_input):
    with open(input_file, 'w+', encoding="utf-8") as file:
        file.write(str_input)


def test1():
    write_and_execute_input_data("1,2,3,5")
    main(input_file, output_file)
    result = "Sample [1. 2. 3. 5.] is classified as class Verginica"
    assert open(output_file, 'r+', encoding="utf-8").readline() == result


def test2():
    write_and_execute_input_data("1.6,0.2,1.1,4.2")
    main(input_file, output_file)
    result = "Sample [1.6 0.2 1.1 4.2] is classified as class Verginica"
    assert open(output_file, 'r+', encoding="utf-8").readline() == result


def test3():
    write_and_execute_input_data("1.1,0.23,3.1,2.22")
    main(input_file, output_file)
    result = "Sample [1.1  0.23 3.1  2.22] is classified as class Verginica"
    assert open(output_file, 'r+', encoding="utf-8").readline() == result


def test4():
    write_and_execute_input_data("0.23,5.22,3.23,2.22")
    main(input_file, output_file)
    result = "Sample [0.23 5.22 3.23 2.22] is classified as class Versicolor"
    assert open(output_file, 'r+', encoding="utf-8").readline() == result


if __name__ == "__main__":
    test1()
    test2()
    test3()
    test4()
